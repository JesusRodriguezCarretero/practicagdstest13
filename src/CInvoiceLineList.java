
public class CInvoiceLineList extends CList {
	public void PushBack(CInvoiceLine e) {
		super.PushBack(e);
	}
	public float totalLinies() {
		float totalLinies = 0.0f;
		CNode node = m_Start;
		while (node != null) {
			CInvoiceLine line = (CInvoiceLine) node.m_Element;
			totalLinies += line.product.m_Price * line.quantity;
			node = node.m_Next;
		}
		return totalLinies;
	}
}